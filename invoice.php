<?php

require 'fpdf/fpdf.php';

// VARIABLES
/////////////
if(isset($_POST['name'])){
	$customer_name = $_POST['name'];
}else{
	$customer_name = "No name";
}

if(isset($_POST['address'])){
	$customer_address = $_POST['address'];	
}else{
	$customer_address = "No Address";
}

if(isset($_POST['city'])){
	$customer_city = $_POST['city'];
}else{
	$customer_city = "No City";
}

if(isset($_POST['item1'])){
	$item1 = $_POST['item1'];	
}else{
	$item1 = "No item";
}

if(isset($_POST['item2'])){
	$item2 = $_POST['item2'];	
}else{
	$item2 = "No item";
}



if(isset($_POST['quantity1'])){
	$quantity1 = $_POST['quantity1'];
}else{
	$quantity1 = 0;
}

if(isset($_POST['quantity2'])){
	$quantity2 = $_POST['quantity2'];
}else{
	$quantity2 = 0;
}


$price1 = 1000;
$price2 = 1000;

$total = $quantity1*$price1 + $quantity2*$price2;

$tax = $total*0.25;

$totalWithTax = $total + $tax;

$date = date("d-m-Y");

$invoiceNr = rand(10000, 99999);


// PDF GENERATION
//////////////////

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);

// Company Name top center
$pdf->Cell(180, 10, 'Company Name', 0, 0, 'C');
$pdf->Ln(20);

// Address1-2-3 + Date/Invoice number top left+right
$pdf->SetFontSize(10);
$pdf->Cell(155, 4, $customer_name);
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(10, 4, 'Date: ' . $date);
$pdf->Ln();
$pdf->SetFontSize(10);
$pdf->Cell(155, 4, $customer_address);
$pdf->SetFontSize(8);
$pdf->Cell(10, 4, 'Invoice nr: ' . $invoiceNr);
$pdf->Ln();
$pdf->SetFontSize(10);
$pdf->Cell(0, 4, $customer_city);

//Invoice underline
$pdf->ln(30);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(30, 20, 'INVOICE');
$pdf->Line(10, 85, 145, 85);
$pdf->SetFont('Arial', '', 10);
$pdf->Ln();
$pdf->Cell(120, 5, "$item1 x $quantity1 ($price1 DKK each)");
$pdf->Cell(50, 5, $quantity1*$price1 . ",00");
$pdf->Ln();
$pdf->Cell(120, 5, "$item2 x $quantity2 ($price2 DKK each)");
$pdf->Cell(50, 5, $quantity2*$price2 . ",00");

//Total
$pdf->Ln(30);
$pdf->Line(10, 123, 145, 123);
$pdf->Cell(80, 5, '');
$pdf->Cell(20, 5, 'AMOUNT');
$pdf->Cell(36, 5, $total . ",00", 0, 0, 'R');
$pdf->Ln();
$pdf->Cell(80, 5, '');
$pdf->Cell(20, 5, 'MOMS 25%');
$pdf->Cell(36, 5, $tax . ",00", 0, 0, 'R');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(80, 5, '');
$pdf->Cell(20, 5, 'TOTAL DKK');
$pdf->Cell(36, 5, $totalWithTax . ",00", 0, 0, 'R');
$pdf->Line(91, 140, 145, 140);

// Bank/Account/Payment
$pdf->Ln(20);
$pdf->SetFont('Arial', '', 8);
$pdf->SetTextColor(130, 20, 130);
$pdf->Cell(20, 5, 'BANK X-BANK');
$pdf->Ln();
$pdf->Cell(30, 5, 'ACCOUNT 1234 1234567890');
$pdf->Ln();
$pdf->Cell(50, 5, 'PAYMENT: net 8 days. Interest accrued at 2.0 % per month thereafter');

// Company Name bottom
$pdf->Ln(50);
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetTextColor(0, 0, 0);
$pdf->Cell(150, 5, '');
$pdf->Cell(25, 5, 'Company Name Ltd');
$pdf->SetFont('Arial', '', 7);
$pdf->Ln();
$pdf->Cell(150, 5, '');
$pdf->Cell(25, 4, 'Vejnavn 2');
$pdf->Ln();
$pdf->Cell(150, 5, '');
$pdf->Cell(25, 4, '1234 Bynavn');
$pdf->Ln();
$pdf->Cell(150, 5, '');
$pdf->Cell(25, 4, 'Tlf: 3333 3333');
$pdf->Ln(8);
$pdf->Cell(150, 5, '');
$pdf->Cell(25, 4, 'CVR-nr: 1234 5678');
$pdf->Ln();
$pdf->Cell(150, 5, '');
$pdf->Cell(25, 4, 'www.dit-dom�ne.dk');





$pdf->Output();


?>