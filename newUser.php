<?php

include 'top.php';
include 'newUserForm.php';

if(userEnteredCredentials()){
	$username = $_POST['username'];
	$password = $_POST['password'];
	$pwConfirm = $_POST['passwordConfirm'];
	
	$userIsValid = validateUsername($username);
	$passwordIsValid = validatePassword($password);
	$pwConfirmationIsValid = validatePwConfirmation($password, $pwConfirm);
	
	if(!$userIsValid){
		echo "Username is not a valid e-mail or another user has already registered using that e-mail.<br>";
	}else if(!$passwordIsValid){
		echo "Password invalid. Must contains both letters and numbers. Password length must be between 8 and 24.";
	}else if(!$pwConfirmationIsValid){
		echo "The passwords you entered did not match.";
	}
	
	
	if($userIsValid && $passwordIsValid && $pwConfirmationIsValid){
		buildDatabase();
		saveUserInDB($username, $password);
	}
	
	
}




include 'bottom.php';

function buildDatabase(){
	// create connection
	$con = mysqli_connect("localhost","root","root");
	// check connection
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	// if database does not already exist
	//if(!mysql_select_db('mh2', $con)){
		// then create database
		$sql_db="CREATE DATABASE IF NOT EXISTS mh2";
		if(mysqli_query($con, $sql_db)){
			echo "Database was created!";
		}else{
			echo "Error: " . mysqli_connect_error();
		}
	//}
	
	// connect and select db
	$con = mysqli_connect("localhost", "root", "root", "mh2");
	
	$sql_table="CREATE TABLE IF NOT EXISTS users(email varchar(100) primary key, password varchar(40))";
	
	// Execute query
	mysqli_query($con, $sql_table);
	
	
}

function saveUserInDB($email, $password){
	$con = new mysqli("localhost", "root", "root", "mh2");
	
	if(mysqli_connect_errno()){
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$sql = "INSERT INTO users (email, password) VALUES ('" . $email . "', SHA1('" . $password . "'))";
	
	if(!mysqli_query($con, $sql)){
		die("Error: " . mysqli_error($con));
	}
	echo "<h1> $email is now registered in the database!</h1>";
	
	mysqli_close($con);
	
	
	
}


function userEnteredCredentials(){
	return (boolean) (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['passwordConfirm']));
}

function validateUsername($username){
	return (boolean) filter_var($username, FILTER_VALIDATE_EMAIL);
}

function validatePassword($password){
	// Checks password length (must be 8-24)
	$lengthIsValid = (strlen($password) >= 8 && strlen($password) <= 24);
	// Checks if password contains both letters and numbers
	$containsLettersAndNumbers = (preg_match('/[a-zA-Z]+[0-9]+/', $password) || preg_match('/[0-9]+[a-zA-Z]+/', $password));

	return (boolean) ($lengthIsValid && $containsLettersAndNumbers);
}

function validatePwConfirmation($password, $repeated){
	return (boolean) ($password == $repeated);
}

?>