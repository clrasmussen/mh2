<?php

include 'top.php';

$cities = array("Tokyo", "Mexico City", "New York City", "Mumbai", 
		"Seoul", "Shanghai", "Lagos", "Buenos Aires", "Cairo", "London");


echo "<br>";
printAllCities($cities);
echo "<br>";
sortAndPrintList($cities);
echo "<br>";
addCities($cities);
echo "<br>";
echo "<br>";
echo "<br>";

include 'bottom.php';


function addCities($initalArray){
	$newCities = array("Los Angeles", "Calcutta", "Osaka", "Beijing");
	
	echo "<h3>";
	echo "Adding the following cities: <br>";
	for($i = 0; $i < sizeof($newCities); $i++){
		echo $newCities[$i];
		if($i < (sizeof($newCities)-1)){
			echo ", ";
		}else{
			echo ".";
		}
	}
	
	
	$cities = array_merge($initalArray, $newCities);
	echo "</h3>";
	echo "</br>";
	sortAndPrintList($cities);
}

function printAllCities($cities){
	echo "<h3>";
	echo "We have the following cities in an array: <br>";
	for($i = 0; $i < sizeof($cities); $i++){
		echo $cities[$i];
		if($i < (sizeof($cities)-1)){
			echo ", ";
		}else{
			echo ".";
		}
	}
	echo "</h3>";
}

function sortAndPrintList($cities){
	sort($cities);
	echo "<ul>";
	for($i = 0; $i < sizeof($cities); $i++){
		printListItem($cities[$i]);
	}
	echo "</ul>";
}

function printListItem($item){
	echo "<li>";
	echo $item;
	echo "</li>";
}


?>