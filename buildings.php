<?php

// //////////////////////// //
// ///// SELVE KODEN ////// //
include 'top.php';			//
							//
buildIntro ();				//
buildCheckboxForm ();		//
extractValuesAndPrint ();	//
							//
echo "</br> </br> </br>";	//
include 'bottom.php';		//
							//
// ////// KODE SLUT /////// //
// //////////////////////// //

// //////////////////////// //
// /////// METODER //////// //
// //////////////////////// //

/*
 * Printer indledende tekst
 */
function buildIntro() {
	echo "<h2> Where do you want access to? </h2></br>";
}
/*
 * Indeholder html kode som bygger en form med checkbokse og en submit knap
 */
function buildCheckboxForm() {
	echo '<form action="buildings.php" method="post">';
	echo '<input type="checkbox" name="myform[]" value="Red Building"> Red Building<br/>';
	echo '<input type="checkbox" name="myform[]" value="Tree House"> Tree House<br/>';
	echo '<input type="checkbox" name="myform[]" value="Fraternity Crib"> Fraternity Crib<br/>';
	echo '<input type="checkbox" name="myform[]" value="Utility Baracks"> Utility Baracks<br/>';
	echo '<input type="checkbox" name="myform[]" value="Oak Complex"> Oak Complex<br/><br/>';
	echo '<input type="submit" name="submit" value="Send!">';
	echo '</form>';
}
/*
 * Hvis brugeren har valgt 1 eller flere checkbokse og trykker submit, vil vi baseret p�
 * st�rrelsen af array'et som indeholder checkbox values, kunne fort�lle brugeren
 * hvor meget den samlede pris er.
 */
function extractValuesAndPrint() {
	if (isset ( $_POST ['myform'] )) {
		$checkValues = $_POST ['myform'];
		echo " You selected " . sizeof ( $checkValues ) . " door(s): ";
		foreach ( $checkValues as $value ) {
			echo $value . " ";
		}
		echo "</br>Price is " . sizeof ( $checkValues ) * 10 . " $.";
	} else {
		echo " Please select one or more items.";
	}
}

?>